package praktikum2;

import lib.TextIO;

public class ArvudeKorrutis {

	public static void main(String[] args) {
		
		// TODO Auto-generated method stub
		System.out.println("Palun sisesta kaks arvu");
		System.out.println("Arvude korrutis on:" + TextIO.getlnInt() * TextIO.getlnInt());
		// defineerime muutujad
		int arv1;
		int arv2;
		int korrutis;
		
		System.out.println("Palun sisesta kaks arvu");
		//väärtustame muutujad
		arv1 = TextIO.getlnInt();
		arv2 = TextIO.getlnInt();
		
		korrutis = arv1 * arv2;
		
		System.out.println("Nende arvude korrutis on: " + korrutis);
	}

}
