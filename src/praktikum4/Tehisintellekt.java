package praktikum4;

import lib.TextIO;

public class Tehisintellekt {

	public static void main(String[] args) {

		System.out.println("Palun sisesta kaks vanust");
		int vanus1 = TextIO.getlnInt();
		int vanus2 = TextIO.getlnInt();
		if (vanus1 < 0) {
			System.out.println("");
		}
		int vanusteVahe = Math.abs(vanus1 - vanus2);

		if (vanusteVahe > 10) {
			System.out.println("Midagi kr�bedat");
		} else if (vanusteVahe > 5) {
			System.out.println("No ikka ei sobi");
		} else {
			System.out.println("Sobib");
		}

	}

}
