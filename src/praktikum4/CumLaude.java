package praktikum4;

import lib.TextIO;

public class CumLaude {

	public static void main(String[] args) {

//		System.out.println("Sisesta l�put�� hinne");
//		int l6putoo = TextIO.getlnInt();
//		if (l6putoo < 0 || l6putoo > 5) {
//			System.out.println("See ei ole korrektne l�put�� hinne!");
//			return;
//		}
//		
//		System.out.println("Sisesta keskmine hinne");
//		double keskmine = TextIO.getlnDouble();
//		if (keskmine < 0 || keskmine > 5) {
//			System.out.println("See ei ole korrektne keskmine hinne!");
//			return;
//		}
		
		double keskmine = kysiHinne("Sisesta keskmine hinne");
		int l6putoo = (int) kysiHinne("Sisesta l�put�� hinne");

		// && -- loogiline JA
		// || -- loogiline VÕI
		// ! -- eitus
		
		// == -- võrdlemine
		// = -- omistamine
		
		if (l6putoo == 5 && keskmine >= 4.5) {
			System.out.println("Saad Cum Laude!");
		} else {
			System.out.println("Ei saa Cum Laudet.");
		}

	}

	private static double kysiHinne(String kysimus) {
		System.out.println(kysimus);
		double hinne = TextIO.getlnDouble();
		if (hinne < 0 || hinne > 5) {
			System.out.println("See ei ole korrektne hinne!");
			System.exit(0);
		}
		return hinne;
	}

}